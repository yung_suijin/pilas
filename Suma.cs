using System;
using System.Collections;

namespace pilas
{
    internal class Suma
    {
        public void sumaEnteros(Stack pila1, Stack pila2)
        {
            int suma, acarreo = 0;
            Stack pila3 = new Stack();
            
            while(pila1.Count != 0 || pila2.Count != 0)
            {
                if (pila2.Count == 0)
                {
                    suma = Convert.ToInt32(pila1.Pop()) + acarreo;

                    if (suma >= 10)
                    {
                        suma -= 10;
                        acarreo = 1;
                        pila3.Push(suma);
                        pila3.Push(acarreo);
                    }
                    else
                    {
                        acarreo = 0;
                        pila3.Push(suma);
                    }
                }
                else if (pila1.Count == 0)
                {
                    suma = Convert.ToInt32(pila2.Pop()) + acarreo;

                    if (suma >= 10)
                    {
                        suma -= 10;
                        acarreo = 1;
                        pila3.Push(suma);
                        pila3.Push(acarreo);
                    }
                    else
                    {
                        acarreo = 0;
                        pila3.Push(suma);
                    }
                }
                else
                {
                    suma = Convert.ToInt32(pila1.Pop()) + Convert.ToInt32(pila2.Pop()) + acarreo;

                    if (suma >= 10)
                    {
                        if(pila1.Count == 0 && pila2.Count == 0)
                        {
                            suma -= 10;
                            acarreo = 1;
                            pila3.Push(suma);
                            pila3.Push(acarreo);
                        }
                        else
                        {
                            suma -= 10;
                            acarreo = 1;
                            pila3.Push(suma);
                        }
                    }
                    else
                    {
                        acarreo = 0;
                        pila3.Push(suma);
                    }
                }
            }
            
            Console.Write("La suma es: ");
            
            foreach(int valor in pila3)
                Console.Write(valor);

            Console.WriteLine();
        }

        public void sumaDecimales(Stack pila1, Stack pila2, Stack pila3, Stack pila4)
        {
            int suma, acarreo = 0, valor = 0;
            Stack pila5 = new Stack();
            Stack pila6 = new Stack();

            while (pila3.Count != 0 || pila4.Count != 0)
            {
                if (pila3.Count > pila4.Count)
                {
                    valor = Convert.ToInt32(pila3.Pop());
                    pila6.Push(valor);
                }
                else if (pila3.Count < pila4.Count)
                {
                    valor = Convert.ToInt32(pila4.Pop());
                    pila6.Push(valor);
                }
                else
                {
                    suma = Convert.ToInt32(pila3.Pop()) + Convert.ToInt32(pila4.Pop()) + acarreo;

                    if (suma >= 10)
                    {
                        suma -= 10;
                        acarreo = 1;
                        pila6.Push(suma);
                    }
                    else
                    {
                        acarreo = 0;
                        pila6.Push(suma);
                    }
                }
            }

            while(pila1.Count != 0 || pila2.Count != 0)
            {
                if (pila2.Count == 0)
                {
                    suma = Convert.ToInt32(pila1.Pop()) + acarreo;

                    if (suma >= 10)
                    {
                        suma -= 10;
                        acarreo = 1;
                        pila5.Push(suma);
                        pila5.Push(acarreo);
                    }
                    else
                    {
                        acarreo = 0;
                        pila5.Push(suma);
                    }
                }
                else if (pila1.Count == 0)
                {
                    suma = Convert.ToInt32(pila2.Pop()) + acarreo;

                    if (suma >= 10)
                    {
                        suma -= 10;
                        acarreo = 1;
                        pila5.Push(suma);
                        pila5.Push(acarreo);
                    }
                    else
                    {
                        acarreo = 0;
                        pila5.Push(suma);
                    }
                }
                else
                {
                    suma = Convert.ToInt32(pila1.Pop()) + Convert.ToInt32(pila2.Pop()) + acarreo;

                    if (suma >= 10)
                    {
                        if(pila1.Count == 0 && pila2.Count == 0)
                        {
                            suma -= 10;
                            acarreo = 1;
                            pila5.Push(suma);
                            pila5.Push(acarreo);
                        }
                        else
                        {
                            suma -= 10;
                            acarreo = 1;
                            pila5.Push(suma);
                        }
                    }
                    else
                    {
                        acarreo = 0;
                        pila5.Push(suma);
                    }
                }
            }

            Console.Write("La suma es: ");

            foreach (int item in pila5)
                Console.Write(item);

            Console.Write(".");

            foreach (int item2 in pila6)
                Console.Write(item2);

            Console.WriteLine();
        }
    }
}