﻿using System;
using System.Collections;

namespace pilas
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcion;
            string n, m;
            Stack pila1 = new Stack();
            Stack pila2 = new Stack();
            Stack pila3 = new Stack();
            Stack pila4 = new Stack();
            int[] arregloPila1;
            int[] arregloPila2;
            int[] arregloPila3;
            int[] arregloPila4;

            var suma = new Suma();

            Console.WriteLine("1. Suma enteros.");
            Console.WriteLine("2. Suma decimales.");
            opcion = Int32.Parse(Console.ReadLine());

            switch (opcion)
            {
                case 1:
                    Console.Write("\nIngrese los digitos para la primera cifra: ");
                    n = Console.ReadLine();

                    char[] primeraCifra = n.ToCharArray();
                    arregloPila1 = Array.ConvertAll(primeraCifra, c => (int)Char.GetNumericValue(c));

                    for (int i = 0; i < arregloPila1.Length; i++)
                        pila1.Push(arregloPila1[i]);

                    Console.Write("\nIngrese los digitos para la segunda suma: ");
                    m = Console.ReadLine();

                    char[] segundaCifra = m.ToCharArray();
                    arregloPila2 = Array.ConvertAll(segundaCifra, c => (int)Char.GetNumericValue(c));

                    for (int i = 0; i < arregloPila2.Length; i++)
                        pila2.Push(arregloPila2[i]);

                    suma.sumaEnteros(pila1, pila2);
                    break;

                case 2:
                    Console.WriteLine("Ingrese los digitos para la primera cifra: ");
                    n = Console.ReadLine();

                    char[] primeraCifra1 = n.Split('.')[0].ToCharArray();
                    char[] segundaCifra1 = n.Split('.')[1].ToCharArray();

                    arregloPila1 = Array.ConvertAll(primeraCifra1, c => (int)Char.GetNumericValue(c));
                    arregloPila3 = Array.ConvertAll(segundaCifra1, c => (int)Char.GetNumericValue(c));

                    for (int i = 0; i < arregloPila1.Length; i++)
                        pila1.Push(arregloPila1[i]);

                    for (int i = 0; i < arregloPila3.Length; i++)
                        pila3.Push(arregloPila3[i]);

                    Console.WriteLine("Ingrese los digitos para la segunda cifra: ");
                    m = Console.ReadLine();

                    char[] primeraCifra2 = m.Split('.')[0].ToCharArray();
                    char[] segundaCifra2 = m.Split('.')[1].ToCharArray();

                    arregloPila2 = Array.ConvertAll(primeraCifra2, c => (int)Char.GetNumericValue(c));
                    arregloPila4 = Array.ConvertAll(segundaCifra2, c => (int)Char.GetNumericValue(c));

                    for (int i = 0; i < arregloPila2.Length; i++)
                        pila2.Push(arregloPila2[i]);

                    for (int i = 0; i < arregloPila4.Length; i++)
                        pila4.Push(arregloPila4[i]);

                    suma.sumaDecimales(pila1, pila2, pila3, pila4);
                    break;
            }
        }
    }
}